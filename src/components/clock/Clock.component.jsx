import React,{ useState, useEffect } from "react";

import "./Clock.css"

function Clock() {

    const [timestart, setTimestart] = useState(false)
    const [sp1, setsp1] = useState(0)
    const [sp2, setsp2] = useState(0)
    const [sp3, setsp3] = useState(0)
    // const [x,setx]=useState([])

    const [mill, setmill] = useState(0)
    const [sec, setsec] = useState(0)
    const [min, setmin] = useState(0)
    const [mill1, setmill1] = useState(0)
    const [sec1, setsec1] = useState(0)
    const [min1, setmin1] = useState(0)
    const [mill2, setmill2] = useState(0)
    const [sec2, setsec2] = useState(0)
    const [min2, setmin2] = useState(0)
    const [st1, setst1] = useState("Start")
    const [st2, setst2] = useState("Reset")
    var [arr, setArr] = useState([])
    const [lap, setlap] = useState(1)
    // const [first, setfirst] = useState(second)


    useEffect(() => {
        let interval1=null;
        let interval2=null;
        let interval3=null; 
        let interval4=null;
        let interval5=null;
        let interval6=null; 
        let interval7=null;
        let interval8=null;
        let interval9=null; 


        if(timestart===true)
        {

            setst1("Stop")
            setst2("Lap")
            interval1=setInterval(() => {
                setmill(mill=>(mill+1)%100);
            }, 10);
            interval2=setInterval(() => {
                setsec(sec=>(sec+1)%60)
            }, 1000);
            interval3=setInterval(() => {
                setmin(min=>(min+1)%60)
            }, 60000);


            interval4=setInterval(() => {
                setmill1(mill1=>(mill1+1)%100);
            }, 10);
            interval5=setInterval(() => {
                setsec1(sec1=>(sec1+1)%60)
            }, 1000);
            interval6=setInterval(() => {
                setmin1(min1=>(min1+1)%60)
            }, 60000);

            interval7=setInterval(() => {
                setmill2(mill2=>(mill2+1)%100);
            }, 10);
            interval8=setInterval(() => {
                setsec2(sec2=>(sec2+1)%60)
            }, 1000);
            interval9=setInterval(() => {
                setmin2(min2=>(min2+1)%60)
            }, 60000);
        }
        else{
            setst1("Start")
            setst2("Reset")
            clearInterval(interval1)
            clearInterval(interval2)
            clearInterval(interval3)
            clearInterval(interval4)
            clearInterval(interval5)
            clearInterval(interval6)
            clearInterval(interval7)
            clearInterval(interval8)
            clearInterval(interval9)

        }

        return ()=>{
            clearInterval(interval1);
            clearInterval(interval2);
            clearInterval(interval3);
            clearInterval(interval4)
            clearInterval(interval5)
            clearInterval(interval6)
            clearInterval(interval7)
            clearInterval(interval8)
            clearInterval(interval9)
        }
    
      
    }, [timestart])
    
    const lapfun=()=>{
        
        if(st2==="Lap")
        {
            var a;
            var x;
            setmill2(0)
            setsec2(0)
            setmin2(0)
           
        
            // if(mill2<10 && sec2<10)
            // {
            //     a1=min2+":"+"0"+sec2+":"+"0"+mill2;
            // }
            if(min2<10 && sec2<10 && mill2<10)
            {
                x="0"+min2+":"+"0"+sec2+":"+"0"+mill2
            }
            else if(min2<10 && sec2<10 && mill2>10)
            {
                x="0"+min2+":"+"0"+sec2+":"+mill2
            }
            else if(min2<10 && sec2>10 && mill2<10)
            {
                x="0"+min2+":"+sec2+":"+"0"+mill2
            }
            else if(min2<10 && sec2>10 && mill2>10)
            {
                x="0"+min2+":"+sec2+":"+mill2
            }
            else if(min2>10 && sec2<10 && mill2<10)
            {
                x=min2+":"+"0"+sec2+":"+"0"+mill2
            }
            else if(min2>10 && sec2<10 && mill2>10)
            {
                x=min2+":"+"0"+sec2+":"+mill2
            }
            else if(min2>10 && sec2>10 && mill2<10)
            {
                x=min2+":"+sec2+":"+"0"+mill2
            }
            else if(min2>10 && sec2>10 && mill2>10)
            {
                x=min2+":"+sec2+":"+mill2
            }

            if(sec<10 && min<10)
            {
                if(mill<10)
                {
                    a=[lap,x,"0"+min+":"+"0"+sec+":"+"0"+mill]
                }
                else{
                    a=[lap,x,"0"+min+":"+"0"+sec+":"+mill]
                }
                
                arr.unshift(a)
                setArr(arr)
            }
            else if(sec>10 && min<10)
            {
                if(mill<10)
                {
                    a=[lap,x,"0"+min+":"+sec+":"+"0"+mill]
                }
                else 
                {
                    a=[lap,x,"0"+min+":"+sec+":"+mill]
                }
                
                arr.unshift(a)
                setArr(arr)
            }
            else if(sec<10 && min>10)
            {
                if(mill<10){
                    a=[lap,x,min+":"+"0"+sec+":"+"0"+mill]
                }
                else{
                    a=[lap,x,min+":"+"0"+sec+":"+mill]
                }
                
                arr.unshift(a)
                setArr(arr)
            }
            else if(sec>10 && min>10)
            {
                if(mill<10)
                {
                    a=[lap,x,min+":"+sec+":"+"0"+mill]
                }
                else{
                    a=[lap,x,min+":"+sec+":"+mill]
                }
                
                arr.unshift(a)
                setArr(arr)
            }
            console.log(arr)
            setlap(lap+1)
            
        }
        else{
            setlap(1)
            setmill(0);
            setsec(0);
            setmin(0);
            setmill1(0);
            setsec1(0);
            setmin1(0);
            setArr([])
        }
    }

    const start=()=>{

        if(timestart===false)
        {
            setTimestart(true)
        }
        else{
            setTimestart(false)
            setmill2(0)
            setsec2(0)
            setmin2(0)
        }
    }


  return (<>
    <div className="">
        
        <h3>{min<10?"0"+min:min}:{sec<10?"0"+sec:sec}:{mill<10?"0"+mill:mill}</h3>
        <br />
        
        <div className="row1">
            <div className="col1">
            <button className="btn btn-outline-danger" onClick={()=>{start();}}>
            {st1}
            </button>

            </div>
            <div className="col1">
            <button className="btn btn-outline-danger" onClick={()=>{lapfun();}}>
            {st2}
            </button>
            </div>
        </div>
<div><br />
<div className="row1">
    <div className="col1">
        Lap Time
    </div>
    <div className="col1">
        Lap
    </div>
    <div className="col1">
Total Time
    </div> 
</div>

<div className="row1">
<div className="col1"> {min2<10?"0"+min2:min2}:{sec2<10?"0"+sec2:sec2}:{mill2<10?"0"+mill2:mill2}</div>
<div className="col1">
{lap<9?"Lap#0"+lap:"Lap#"+lap}
</div>
<div className="col1">
{min1<10?"0"+min1:min1}:{sec1<10?"0"+sec1:sec1}:{mill1<10?"0"+mill1:mill1}
</div>

</div>

<div>
    {
        arr.map((value)=>{
        //    setx(value[1].split(":"));
            // if(x[0]<10)
            // {
            //     setsp1("0"+x[0])
            // }
            // else{
            //     setsp1(x[0])
            // }
            // if(x[0]<10)
            // {
            //     setsp2("0"+x[1])
            // }
            // else{
            //     setsp2(x[1])
            // }
            // if(x[0]<10)
            // {
            //     setsp3("0"+x[2])
            // }
            // else{
            //     setsp3(x[2])
            // }
            return(<>
            <div className="row1">
                <div className="col1">
                {value[1]}
                </div>
                <div className="col1">
                {value[0]<10?"Lap#0"+value[0]:"Lap#"+value[0]}
                </div>
                <div className="col1">
                {value[2]}
                </div>
            </div>
            </>)
        })
    }
</div>
</div>
    </div>
</>);
}

export default Clock;
// eslint-disable-next-line