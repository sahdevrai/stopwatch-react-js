import React,{useState} from 'react';
// import './App.css';
// import Clock from './components/clock/Timer.component';

const Timer=()=> {

    const [timestart, setTimestart] = useState(false)
    const [mill, setmill] = useState(0)
    const [sec, setsec] = useState(0)
    const [min, setmin] = useState(0)
    const [mill1, setmill1] = useState(0)
    const [sec1, setsec1] = useState(0)
    const [min1, setmin1] = useState(0)
    const [mill2, setmill2] = useState(0)
    const [sec2, setsec2] = useState(0)
    const [min2, setmin2] = useState(0)
    const [st1, setst1] = useState("Start")
    const [st2, setst2] = useState("Reset")
    var [arr, setArr] = useState([])
    const [lap, setlap] = useState(1)

  return (
    <div className="Timer">
       <h3>{min<10?"0"+min:min}:{sec<10?"0"+sec:sec}:{mill<10?"0"+mill:mill}</h3>
    </div>
  );
}

export default Timer;
