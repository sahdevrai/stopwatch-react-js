import './App.css';
import Clock from './components/clock/Clock.component';
import Timer from './components/timer/Timer.component';

function App() {
  return (
    <div className="App">
      <div className='display-4 heading'>
        Stop Watch
      </div><br />
      
      <Clock/>
    </div>
  );
}

export default App;
